# Dokumentasi Angular

Explorasi & Dokumentasi pribadi angular 2+

*Penjelasan lengkap tersedia di [tautan ini](https://doc.mediadesain.com/)

---
## Instalasi
- Pastikan Install NodeJS terlebih Dahulu
- Install Angular2+ CMD/Terminal `npm install -g @angular/cli`
- Jika error jalankan sebagai administator + password `sudo npm install -g @angular/cli`
- Untuk membuat project baru Angular2+ jalankan perintah `ng new mediadesain-web`
- Untuk menjalankan project pilih dahulu target path `cd mediadesain-web`
- Untuk menjalankan server localhost `ng serve`
- Proses Devlopment [Mulai dari layouting, routing, service parsing data etc]
- Untuk mengcomplie output production jalankan `ng build`, output berada difolder `dist`
---

## Explorasi tentang Modul & Komponen
1. [Struktur Komponen & Modul](https://doc.mediadesain.com/angular/pemahaman-komponen-modul)
2. [Contoh Routing Modul](https://doc.mediadesain.com/angular/routing-modul)
3. [Contoh Komponen Pisah](https://doc.mediadesain.com/angular/komponen-terpisah)
4. [Contoh Komponen Gabung](https://doc.mediadesain.com/angular/komponen-gabung)
5. [Contoh routing dengan lazy loading](https://doc.mediadesain.com/angular/routing-modul-detail)

---

## Explorasi tentang Binding Data
1. [Interpolation {{}}](https://doc.mediadesain.com/angular/binding-data)
2. [Property []](https://doc.mediadesain.com/angular/binding-data)
3. [Event Binding ()](https://doc.mediadesain.com/angular/event-binding)
3. [TwoWay Binding [()]](https://doc.mediadesain.com/angular/twoway-binding)

---

## Explorasi tentang Directive
- [Dynamic class - ngClass](https://doc.mediadesain.com/angular/ng-class)
- [Loop Items - ngFor](https://doc.mediadesain.com/angular/ng-for)
- [Render base on condition - ngIf](https://doc.mediadesain.com/angular/ng-if)
